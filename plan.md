---
layout: "page"
title: "Plan d'Accès"
icon: fa-map-marked-alt
order: 3
permalink: /plan.html
---

La JRDA aura lieu au centre Pierre Guillaumat du campus sud de l'UTC.

{% include download_button.html path="assets/download/plan.pdf" text="Plan d'accès UTC" %}

{% include openstreetmap.html %}

### Transports en commun
La gare de compiègne est desservie par les lignes d'Amiens, Saint-Quentin et Paris.
Ensuite, prendre la ligne 5 à la sortie de la gare (bus toutes les 15 minutes) et descendre à l'arrêt `Centre de Recherche`. Le site [Oise Mobilité](https://www.oise-mobilite.fr/fr/itineraires/4/JourneyPlanner/result?PointDep=75_4&LatDep=49.4217643268083&LngDep=2.82382779365&PointArr=835_4&LatArr=49.40088014605&LngArr=2.7994789966) détaille l'itinéraire.

### Voiture
Prendre l'A1 et sortir à la sortie n°10 direction Compiègne.
