---
layout: "page"
title: "Fichiers"
icon: fa-solid fa-file-alt
order: 5
permalink: /templates.html
---

Résumés des posters de l'édition 2021 :

{% include download_button.html path="assets/download/abstracts.pdf" text="Résumés" %}


Modèles proposés pour le résumé et le poster :

{% include download_button.html path="assets/download/poster.ppt" text="Modèle de Poster" %}
{% include download_button.html path="assets/download/abstract.docx" text="Modèle de Résumé" %}
