---
auto-header: none
hide: false
title: "Accueil"
---

Tous les ans, le GRAISyHM, un groupement d'intérêts scientifique soutenu par la région Hauts-de-France, organise une journée pour rassembler les doctorants en automatique, informatique et traitement du signal.

Cette année, la journée est organisée par le laboratoire Heudiasyc de l'Université de Technologie de Compiègne avec comme thème *Emplois d’avenir de nos doctorants en région Hauts-de-France*. Le but de cette journée sera donc de présenter ses travaux de thèse à la communauté régionale et d'échanger sur les différents aspects de la vie de doctorants. Les présentations se feront traditionelement au travers de posters, et des prix de meilleur réalisations seront déscernés.

L'inscription est gratuite mais obligatoire, et se fait [ici](inscriptions.html).
