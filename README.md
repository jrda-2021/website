# JRDA 2020 Website

Website sources for the [2020 Journée Régionale des Doctorants en Automatique](https://jrda2020.hds.utc.fr/). It uses Jekyll, a static site generator, coupled with the Prologue theme.

## Installing Dependencies
Jekyll is based on recent versions of ruby. With Ubuntu 20.04 and up:
```bash
sudo apt install ruby-dev
```

Then install Jekyll and dependencies (in the repository):
```bash
# install the jekyll and bundler packages
sudo gem install jekyll bundler

# Build the theme
gem build _themes/prologue/prologue.gemspec

# Retrieve the site's dependencies
bundle install --path vendor/bundle
```

## Running
To run a test server of the website on the [local machine](https://localhost:4000), run
```bash
bundle exec jekyll serve
```

## Uploading
Commits pushed on the `master` branch are automatically applied to the [public website](https://jrda2020.hds.utc.fr/). Thus, for development steps, please use another branch (`devel` or other). For this, the Gitlab CI is used (described in the `.gitlab-ci.yml`) and the process takes less than 10 minutes to install and compile.

## Modifying
Changing/adding text is trivial. A page's content is described in a markdown file (same language as this readme), where simple formatting is straight-forward. For more complex formatting, it is also possible to write html directly.

## Theme
The theme is a modified version of [Prologue](https://github.com/chrisbobbe/jekyll-theme-prologue), stored in the `_themes` directory. It is possible to modify it but remember to run
```bash
gem build _themes/prologue/prologue.gemspec
bundle update
```
afterwards.
