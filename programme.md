---
layout: "page"
title: "Programme de la Journée"
icon: fa-calendar-alt
order: 2
permalink: /programme.html
---

| 9h15  | 9h30  | Accueil des participants - Café                                                                                      |
|-------|-------|----------------------------------------------------------------------------------------------------------------------|
| 9h30  | 10h00 | Ouverture de la journée : Philippe Bonnifait (Directeur d’Heudiasyc) – Frederic Vanderhaegen (Président du GRAISYHM) |
| 10h00 | 10h30 | "Perspective d’emplois après la thèse", L. Macaire (CRISTAL)                                                         |
| 10h30 | 11h00 | Élu de la section 61 : Fonctionnement et critères de qualification aux fonctions de maîtres de conférences           |
| 11h00 | 12h15 | Témoignages de docteurs : Subeer Rangra (Alstom) - Nicole El-Zoughby (Renault)                                       |

| 12h15 | 13h45 | Pause déjeuner                                                                                                       |
|-------|-------|----------------------------------------------------------------------------------------------------------------------|
| 14h00 | 15h15 | Session poster                                                                                                       |
| 15h15 | 15h30 | Prix du meilleur poster                                                                                              |
| 15h30 | 15h45 | Pause - Café                                                                                                         |
| 15h45 | 16h45 | Visite des plateformes Heudiasyc                                                                                     |
