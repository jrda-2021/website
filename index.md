---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
navtitle: Accueil
title: Journée Régionale des Doctorants en Automatique
icon: fa-home
order: 1
cover-photo: assets/images/cover.jpg
cover-photo-alt: home background
permalink: /
---

19 Novembre 2021 - Compiègne
