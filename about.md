---
layout: page
title: Informations
order: 6
icon: fa-info-circle
permalink: /informations.html
---

<!--# Présentation du GRAISyHM
Lorem Ipsum Dores Dalam-->

# Contacts des JRDA21
* [Par mail](mailto:jrda2021@hds.utc.fr) 

# Comité d'Organisation
* [Mohamed Sallak (Heudiasyc)](mailto:mohamed.sallak@hds.utc.fr)
* [Veronique Cherfaoui (Heudiasyc)](mailto:veronique.cherfaoui@hds.utc.fr)
* [Joelle Al Hage (Heudiasyc)](mailto:joelle.al-hage@hds.utc.fr)
* [Julien Moreau (Heudiasyc)](mailto:julien.moreau@hds.utc.fr)
* [Ghada Jaber (Heudiasyc)](mailto:ghada.jaber@hds.utc.fr)
* [Lounis Adouane (Heudiasyc)](mailto:lounis.adouane@hds.utc.fr)
* [Reine Talj (Heudiasyc)](mailto:reine.talj@hds.utc.fr)
* [Hélène Ballet (Heudiasyc)](mailto:helene.ballet@hds.utc.fr)
* [Antoine Lima (Heudiasyc)](mailto:antoine.lima@hds.utc.fr)
* [Ali Hamdan (Heudiasyc)](mailto:ali.hamdan@hds.utc.fr)
* [Lyes Saidi (Heudiasyc)](mailto:lyes.saidi@hds.utc.fr)
* [Maxime Escourrou (Heudiasyc)](mailto:maxime.escourrou@hds.utc.fr)
* [Baptiste Wojtkowski (Heudiasyc)](mailto:bwojtkow@hds.utc.fr)
* [Ali Raza (ENSAIT)](mailto:ali.raza@ensait.fr)
* [Corentin Ascone (LAMIH)](mailto:corentin.ascone@uphf.fr)
* [Frédéric Vanderhaegen (Président du GRAISyHM)](mailto:frederic.vanderhaegen@uphf.fr)

<!--# Laboratoires participants
* Heudiasyc, UTC, Compiègne
* MIS, UPJV, Amiens
* ...-->
